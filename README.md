# Admiral Kuznetsov aircraft carrier mod for ArmA 3
#
Admiral Flota Sovetskovo Soyuza Kuznetsov (Russian: Адмира́л фло́та Сове́тского Сою́за Кузнецо́в "Admiral of the Fleet of the Soviet Union Kuznetsov") is an aircraft cruiser (heavy aircraft-carrying missile cruiser, or TAVKR, in Russian classification) serving as the flagship of the Russian Navy. She was built by the Black Sea Shipyard, the sole manufacturer of Soviet aircraft carriers, in Mykolaiv within the Ukrainian Soviet Socialist Republic
#
| Class             | Namesake          | Name                                      |
| ----------------- |:-----------------:| -----------------------------------------:|
| 	Kuznetsov-class | Nikolay Kuznetsov | Admiral Flota Sovetskogo Soyuza Kuznetsov |

#
Aircraft carried:	Approx. 41 aircraft
#
| Fixed Wing        | Number            |
| ----------------- |:-----------------:|
| 	Sukhoi Su-33    |         12        |
| 	Mikoyan MiG-29K |         20        |
| 	Sukhoi Su-25UTG |          4        |
#
| Rotary Wing       | Number            |
| ----------------- |:-----------------:|
| 	Kamov Ka-27LD32 |          4        |
| 	Kamov Ka-27PL   |         18        |
| 	Kamov Ka-27PS   |          2        |
#
[![](https://www.paypalobjects.com/en_US/i/btn/btn_donateCC_LG.gif)](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=K6922R75JMFTS)
#
[Download](https://github.com/Nobatgeldi/Admiral-Kuznetsov/zip/master)
#Picture
![GitHub Logo](https://raw.githubusercontent.com/Nobatgeldi/Admiral-Kuznetsov/master/1.jpg)
#
![GitHub Logo](https://raw.githubusercontent.com/Nobatgeldi/Admiral-Kuznetsov/master/2.jpg)
#
![GitHub Logo](https://raw.githubusercontent.com/Nobatgeldi/Admiral-Kuznetsov/master/3.jpg)
#
![GitHub Logo](https://raw.githubusercontent.com/Nobatgeldi/Admiral-Kuznetsov/master/4.jpg)
#
![GitHub Logo](https://raw.githubusercontent.com/Nobatgeldi/Admiral-Kuznetsov/master/5.jpg)
#
![GitHub Logo](https://raw.githubusercontent.com/Nobatgeldi/Admiral-Kuznetsov/master/6.jpg)
#
![GitHub Logo](https://raw.githubusercontent.com/Nobatgeldi/Admiral-Kuznetsov/master/7.jpg)
#
Contact info@nobatgeldi.com
